package com.tsn.stateless;

import javax.ejb.Stateless;
import java.io.Console;
import java.util.ArrayList;
import java.util.List;

//@Stateless(name = "LibrarySessionBeanRemote" ,mappedName = "LibrarySessionBeanRemote")
@Stateless(name = "LibrarySessionBean" ,mappedName = "LibrarySessionBean")
public class LibrarySessionBean implements LibrarySessionBeanRemote{

    List<String> bookShelf;

    public LibrarySessionBean() {
        bookShelf = new ArrayList<String>();
    }

    public void addBook(String bookName) {
        bookShelf.add(bookName);
    }

    public List<String> getBooks() {
        return bookShelf;
    }

    @Override
    public void sayHello(String name) {

        System.out.println("Hello " + name);
    }
}
