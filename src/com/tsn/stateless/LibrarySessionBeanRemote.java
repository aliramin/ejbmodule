package com.tsn.stateless;

import javax.ejb.EJB;
import javax.ejb.Remote;
import java.util.List;

@Remote
//@EJB(name="ejb/MyServiceBean/remote", beanName="MyServiceBean")
public interface LibrarySessionBeanRemote {

    void addBook(String bookName);
    List getBooks();

    void sayHello(String name);
}
